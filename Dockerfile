
FROM php:5.6-fpm-stretch

ENV APP_ENV 'prod'
ENV APP_TMZ 'Europe/Paris'
ENV APP_USR ''
ENV APP_UID ''
ENV APP_GRP ''
ENV APP_GID ''

RUN apt-get update

# PHP

RUN apt-get install -y --no-install-recommends \
    git \
    imagemagick \
    patch \
    bzip2 \
    unzip \
    libxml2-dev \
    libpng-dev

RUN docker-php-ext-install soap
RUN docker-php-ext-install intl
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install gd

COPY ./conf-available/ini/ /usr/local/etc/php/
COPY ./conf-available/zzz-app.conf /usr/local/etc/php-fpm.d/zzz-app.conf

# Composer

RUN curl -sS https://getcomposer.org/installer \
    | php -- --install-dir=/usr/local/bin --filename=composer

# Scripts

COPY ./scripts/ /scripts/
RUN chmod 700 /scripts/*.sh

# Démarrage

WORKDIR /var/www/html
CMD ["/scripts/run.sh"]
